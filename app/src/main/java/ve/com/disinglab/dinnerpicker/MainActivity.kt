package ve.com.disinglab.dinnerpicker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val foodList = arrayListOf("Chinese", "Pizza", "Burger")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addNewButton.setOnClickListener {
            if(newFood.text != null){
                val food = newFood.text.toString()
                foodList.add(food)
                newFood.text.clear()
            }
        }

        decideButton.setOnClickListener {
            val random = Random()
            val randomFood = random.nextInt(foodList.count())
            selectedFood.text = foodList[randomFood]
        }
    }
}
